import { Component, OnInit } from '@angular/core';
import { StaffService } from '../../../services/staff.service';
import { Staff } from '../../../models/staff';
import { EquipeService } from '../../../services/equipe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Equipe } from '../../../models/equipe';

@Component({
  selector: 'app-ajoutstaff',
  templateUrl: './ajoutstaff.component.html',
  styleUrls: ['./ajoutstaff.component.css']
})
export class AjoutstaffComponent implements OnInit {
  staff = {
    type: '',
    description: ''
  };

  equipes:  Equipe[];

  constructor(private router: Router, private apiService: StaffService, private api: EquipeService) { }

  ngOnInit(): void {
    this.api.readEquipes().subscribe((equipes: Equipe[])=>{
      this.equipes = equipes['content'];
      console.log(this.equipes);
    })
  }

  createStaff(p,d,f) {
    const data = {
      type: p,
    description: d,
    };
    
    this.apiService.createStaff(data, f).subscribe((Staff: Staff)=>{
      console.log("Staff created, ", Staff);
    });

    this.router.navigate(['./base/staff']).then(() => {
      window.location.reload();
    });

}

}
