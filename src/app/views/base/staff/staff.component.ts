import { Component, OnInit } from '@angular/core';
import { StaffService } from '../../../services/staff.service';
import { Staff } from '../../../models/staff';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  
  staffs:  Staff[];
  constructor(private apiService: StaffService) { }

  ngOnInit(): void {
    this.apiService.readStaffs().subscribe((staffs: Staff[])=>{
      this.staffs = staffs['content'];
      console.log(this.staffs);
    })
  }

  updateStaff(id, p , d) {
    const data = {
      type: p,
      description: d
    };
    console.log(data);
    this.apiService.updateStaff(id, data).subscribe((staff: Staff)=>{
      console.log("staff updated" , staff);
    });
    location.reload();
  }
  
  deleteStaff(id){
    console.log(id);
    this.apiService.deleteStaff(id).subscribe((staff: Staff)=>{
      console.log("staff supprimée, ", staff);
    });
    location.reload();
  }

}
