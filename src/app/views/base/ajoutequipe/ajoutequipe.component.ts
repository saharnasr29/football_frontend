import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../../../services/equipe.service';
import { Equipe } from '../../../models/equipe';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ajoutequipe',
  templateUrl: './ajoutequipe.component.html',
  styleUrls: ['./ajoutequipe.component.css']
})
export class AjoutequipeComponent implements OnInit {
  eq = {
    pays: '',
    description: '',
  };

  constructor(private router: Router, private apiService: EquipeService) { }

  ngOnInit(): void {
  }

  createEquipe(p,d) {
    const data = {
      pays: p,
      description: d
    };
    console.log(data);
    this.apiService.createEquipe(data).subscribe((equipe: Equipe)=>{
      console.log("Equipe created, ", equipe);
    });

    this.router.navigate(['./base/equipe']).then(() => {
      window.location.reload();
    });

} 
}
