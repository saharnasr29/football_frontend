import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../../../services/equipe.service';
import { Equipe } from '../../../models/equipe';


@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.component.html',
  styleUrls: ['./equipe.component.css']
})
export class EquipeComponent implements OnInit {

  equipes:  Equipe[];
  
  constructor(private apiService: EquipeService) { }

  ngOnInit(): void {
    this.apiService.readEquipes().subscribe((equipes: Equipe[])=>{
      this.equipes = equipes['content'];
      console.log(this.equipes);
    })


}

updateEquipe(id, p , d) {
  const data = {
    pays: p,
    description: d
  };
  console.log(data);
  this.apiService.updateEquipe(id, data).subscribe((equipe: Equipe)=>{
    console.log("Equipe updated" , equipe);
  });
  location.reload();
}

deleteEquipe(id){
  console.log(id);
  this.apiService.deleteEquipe(id).subscribe((equipe: Equipe)=>{
    console.log("Equipe supprimée, ", equipe);
  });
  location.reload();
}
}