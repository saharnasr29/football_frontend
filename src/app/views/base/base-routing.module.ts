import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { EquipeComponent } from './equipe/equipe.component';
import { StaffComponent } from './staff/staff.component';
import { JoueurComponent } from './joueur/joueur.component';
import { AjoutequipeComponent } from './ajoutequipe/ajoutequipe.component';
import { AjoutstaffComponent } from './ajoutstaff/ajoutstaff.component';
import { AjoutjoueurComponent } from './ajoutjoueur/ajoutjoueur.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Football'
    },
    children: [

    

      {
        path: 'equipe',
        component: EquipeComponent,
        data: {
          title: 'Equipes'
        }
      },
      {
        path: 'ajouteq',
        component: AjoutequipeComponent,
        data: {
          title: 'Ajouter une équipe'
        }
      },

      {
        path: 'ajoutstaff',
        component: AjoutstaffComponent,
        data: {
          title: 'Ajouter un staff'
        }
      },

      {
        path: 'ajoutjoueur',
        component: AjoutjoueurComponent,
        data: {
          title: 'Ajouter un joueur'
        }
      },

      {
        path: 'joueur',
        component: JoueurComponent,
        data: {
          title: 'Joueurs'
        }
      },

      {
        path: 'staff',
        component: StaffComponent,
        data: {
          title: 'Staff'
        }
      },

     

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule {}
