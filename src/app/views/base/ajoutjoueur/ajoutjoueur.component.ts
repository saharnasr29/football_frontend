import { Component, OnInit } from '@angular/core';
import { JoueurService } from '../../../services/joueur.service';
import { Joueur } from '../../../models/joueur';
import { EquipeService } from '../../../services/equipe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Equipe } from '../../../models/equipe';

@Component({
  selector: 'app-ajoutjoueur',
  templateUrl: './ajoutjoueur.component.html',
  styleUrls: ['./ajoutjoueur.component.css']
})
export class AjoutjoueurComponent implements OnInit {
  joueur = {
    nom: '',
    prenom: '',
    position: '',
  };

  equipes:  Equipe[];

  constructor(private router: Router, private apiService: JoueurService, private api: EquipeService) { }

  ngOnInit(): void {
    this.api.readEquipes().subscribe((equipes: Equipe[])=>{
      this.equipes = equipes['content'];
      console.log(this.equipes);
    })
    
  } 

  createJoueur(p,d,e,f) {
    const data = {
      nom: p,
    prenom: d,
    position: f,
    };
    
    this.apiService.createJoueur(data, f).subscribe((Joueur: Joueur)=>{
      console.log("Joueur created, ", Joueur);
    });

    this.router.navigate(['./base/joueur']).then(() => {
      window.location.reload();
    });

}

}
