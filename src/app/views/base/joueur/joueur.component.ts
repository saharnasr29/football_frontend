import { Component, OnInit } from '@angular/core';
import { JoueurService } from '../../../services/Joueur.service';
import { Joueur } from '../../../models/Joueur';

@Component({
  selector: 'app-joueur',
  templateUrl: './joueur.component.html',
  styleUrls: ['./joueur.component.css']
})
export class JoueurComponent implements OnInit {

  joueurs:  Joueur[];
  constructor(private apiService: JoueurService) { }

  ngOnInit(): void {
    this.apiService.readJoueurs().subscribe((joueurs: Joueur[])=>{
      console.log(joueurs);
      this.joueurs = joueurs['content'];
      console.log(this.joueurs);
    })
  }

  updateJoueur(id, p , d , h) {
    const data = {
      nom: p,
      prenom: d,
      position: h
    };
    console.log(data);
    console.log(h);
    this.apiService.updateJoueur(id, data).subscribe((Joueur: Joueur)=>{
      console.log("Joueur updated" , Joueur);
    });
    location.reload();
  }
  
  deleteJoueur(id){
    console.log(id);
    this.apiService.deleteJoueur(id).subscribe((Joueur: Joueur)=>{
      console.log("Joueur supprimée, ", Joueur);
    });
    location.reload();
  }

}
