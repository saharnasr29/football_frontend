import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [

  {
    name: 'Football',
    url: '/base',
    icon: 'icon-puzzle',
    children: [
     
      {
        name: 'Equipes',
        url: '/base/equipe',
        icon: 'icon-puzzle'
      },
      {
        name: 'Joueurs',
        url: '/base/joueur',
        icon: 'icon-puzzle'
      },
      {
        name: 'Staffs',
        url: '/base/staff',
        icon: 'icon-puzzle'
      },
     
    ]
  },
 


];
