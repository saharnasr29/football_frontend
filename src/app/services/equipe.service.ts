import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Equipe } from  '../models/equipe';
import { Observable } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  
     headers = new HttpHeaders({
        
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('currentUser'), });
     options = { headers: this.headers };


    

    API_SERVER = "http://localhost:8080";
  constructor(private httpClient: HttpClient) {}

  

  readEquipes(): Observable<Equipe[]>{
      console.log(this.options);
    return this.httpClient.get<Equipe[]>(`${this.API_SERVER}/equipes`, this.options);

  }

  createEquipe(equipe: Equipe): Observable<Equipe>{
    return this.httpClient.post<Equipe>(`${this.API_SERVER}/equipes`, equipe, this.options);
  }

  updateEquipe(id: number, equipe: Equipe){
    return this.httpClient.put<Equipe>(`${this.API_SERVER}/equipes/${id}`, equipe, this.options);   
  }

  deleteEquipe(id: number){
    return this.httpClient.delete<Equipe>(`${this.API_SERVER}/equipes/${id}`, this.options);
  }

}