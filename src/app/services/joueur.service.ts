import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Joueur } from  '../models/joueur';
import { Observable } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JoueurService {

  headers = new HttpHeaders({
        
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('currentUser'), });

 options = { headers: this.headers };

API_SERVER = "http://localhost:8080";

  constructor(private httpClient: HttpClient) { }

  readJoueurs(): Observable<Joueur[]>{
  return this.httpClient.get<Joueur[]>(`${this.API_SERVER}/joueurs`, this.options);

}

createJoueur(Joueur: Joueur, id : number): Observable<Joueur>{
  return this.httpClient.post<Joueur>(`${this.API_SERVER}/equipes/${id}/joueurs`, Joueur, this.options);
}

updateJoueur(id: number, Joueur: Joueur){
  return this.httpClient.put<Joueur>(`${this.API_SERVER}/joueurs/${id}`, Joueur, this.options);   
}

deleteJoueur(id: number){
  return this.httpClient.delete<Joueur>(`${this.API_SERVER}/joueurs/${id}`, this.options);
}

}
