import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Staff } from  '../models/staff';
import { Observable } from  'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StaffService {

  headers = new HttpHeaders({
        
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('currentUser'), });

 options = { headers: this.headers };

API_SERVER = "http://localhost:8080";

  constructor(private httpClient: HttpClient) { }

  readStaffs(): Observable<Staff[]>{
    return this.httpClient.get<Staff[]>(`${this.API_SERVER}/staffs`, this.options);
  
  }
  
  createStaff(Staff: Staff, id : number): Observable<Staff>{
    return this.httpClient.post<Staff>(`${this.API_SERVER}/equipes/${id}/staffs`, Staff, this.options);
  }
  
  updateStaff(id: number, Staff: Staff){
    return this.httpClient.put<Staff>(`${this.API_SERVER}/staffs/${id}`, Staff, this.options);   
  }
  
  deleteStaff(id: number){
    return this.httpClient.delete<Staff>(`${this.API_SERVER}/staffs/${id}`, this.options);
  }
  

}
