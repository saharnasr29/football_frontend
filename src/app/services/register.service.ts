import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RegisterService {
    constructor(private http: HttpClient) { }

    register(username: string, password: string) {

        return this.http.post<any>(`http://localhost:8080/register`, { username, password })
            .pipe(map(user => {
                // register successful if there's a user in the response
                if (user) {
                    // show user in console

                    console.log(user);

                }

                return user;
            }));

    }


}
